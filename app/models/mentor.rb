class Mentor < ApplicationRecord
  validates :name, presence: { message: 'must be provided.' }, uniqueness: true
  has_many :trainees
end
