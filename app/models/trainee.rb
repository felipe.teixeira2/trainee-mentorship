class Trainee < ApplicationRecord
  validates :name, presence: { message: 'must be provided.' }, uniqueness: true
  belongs_to :mentor, optional: true
end
