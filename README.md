# README

This web application assigns trainees to mentors. Mentors are responsible for reviewing Pull Requests created and posted by trainees. Pull Requests are organized in Trello boards (one for each mentor) which are also linked to in this web application.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
