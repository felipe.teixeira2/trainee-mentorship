# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_02_06_145538) do

  create_table "mentors", force: :cascade do |t|
    t.string "name"
    t.string "board"
    t.integer "trainees_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["trainees_id"], name: "index_mentors_on_trainees_id"
  end

  create_table "trainees", force: :cascade do |t|
    t.string "name"
    t.integer "mentor_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mentor_id"], name: "index_trainees_on_mentor_id"
  end

  add_foreign_key "mentors", "trainees", column: "trainees_id"
  add_foreign_key "trainees", "mentors"
end
