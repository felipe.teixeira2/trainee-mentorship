class CreateTrainees < ActiveRecord::Migration[6.1]
  def change
    create_table :trainees do |t|
      t.string :name
      t.references :mentor, foreign_key: true , null: true

      t.timestamps
    end
  end
end
