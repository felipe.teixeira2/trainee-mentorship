class CreateMentors < ActiveRecord::Migration[6.1]
  def change
    create_table :mentors do |t|
      t.string :name
      t.string :board
      t.references :trainees, foreign_key: true, null: true 

      t.timestamps
    end
  end
end
