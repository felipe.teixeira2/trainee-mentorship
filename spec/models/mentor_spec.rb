require 'rails_helper'

RSpec.describe Mentor, type: :model do

  fixtures :mentors, :trainees

  describe 'validations' do

    describe 'mentor' do

      it 'must have a name' do
        expect(mentors(:nameless)).not_to be_valid
      end

      it 'name must be unique' do
        expect(mentors(:namefellow)).not_to be_valid
      end

      it 'may have trainees assigned' do
        expect(mentors(:busymentor)).to be_valid
      end

      it 'may have no trainees assigned' do
        expect(mentors(:freementor)).to be_valid
      end

      it 'may have a linked Trello board' do
        expect(mentors(:linked)).to be_valid
      end

      it 'may not have a linked Trello board' do
        expect(mentors(:unlinked)).to be_valid
      end

    end

  end

  describe 'database' do

    it 'has seven mentors' do
      expect(mentors.count).to eq(7)
    end
  end

end
