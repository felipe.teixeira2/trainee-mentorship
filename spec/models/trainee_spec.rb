require 'rails_helper'

RSpec.describe Trainee, type: :model do

  fixtures :mentors, :trainees

  describe 'validations' do

    describe 'trainee' do

      it 'must have a name' do
        expect(trainees(:nameless)).not_to be_valid
      end

      it 'name must be unique' do
        expect(trainees(:namefellow)).not_to be_valid
      end

      it 'may have a mentor' do
        expect(trainees(:mentored)).to be_valid
      end

      it 'may have no mentor' do
        expect(trainees(:unmentored)).to be_valid
      end

    end

  end

  describe 'database' do

    it 'has five trainees' do
      expect(trainees.count).to eq(5)
    end
  end
end

